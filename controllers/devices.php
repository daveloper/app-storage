<?php

/**
 * Storage devices controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Storage devices controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

class Devices extends ClearOS_Controller
{
    /**
     * Storage mappings overview.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');

        // Load view data
        //---------------

        try {
            $data['devices'] = $this->storage_device->get_devices();
            $found = $this->storage_device->find_obvious_storage_device();

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        // If wizard and there's disk ready for storage (e.g. Amazon EBS), then 
        // jump right to the create page.

        $no_redirect = $this->session->userdata('storage_noredirect') ? TRUE : FALSE;

        if ($this->session->userdata('wizard') && !$no_redirect && !empty($found)) {
            $this->session->set_userdata('storage_noredirect', TRUE);
            redirect('/storage/devices/view/' . strtr(base64_encode($found),  '+/=', '-_.'));
        } else {
            $this->page->view_form('devices/summary', $data, lang('storage_devices'));
        }
    }

    /**
     * Creates a data drive.
     *
     * @param string $device encoded device name
     *
     * @return view
     */

    function create_data_drive($device)
    {
        
        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        // Handle form submit
        //-------------------

        if ($this->input->post('submit')) {

            try {

                $this->storage_device->run_create_data_drive($device_decoded, $this->input->post('type'));
                $this->page->set_status_added();

            } catch (Exception $e) { 
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the views
        //---------------

        $this->page->view_form('create', $data, lang('base_create'));
    }

    /**
     * Initializes storage engine.
     *
     * @return void.
     */

    function set_initialized()
    {
        // Load dependencies
        //------------------

        $this->load->library('storage/Storage');

        // Set initialized
        //----------------

        $this->storage->set_initialized();
        redirect($this->session->userdata['wizard_redirect']);
    }

    /**
     * Returns create data store state.
     *
     * @return JSON crate data store state
     */

    function get_data_drive_state()
    {
        // Load dependencies
        //------------------

        $this->load->library('storage/Storage_Device');

        // Get state
        //----------

        try {
            $data['state'] = $this->storage_device->get_data_drive_state();
            $data['error_code'] = 0;
        } catch (Exception $e) {

            $data['error_code'] = clearos_exception_code($e);
            $data['error_message'] = clearos_exception_message($e);
        }

        // Return state
        //-------------

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Fri, 01 Jan 2010 05:00:00 GMT');
        header('Content-type: application/json');

        $this->output->set_output(json_encode($data));
    }


    /**
     * Storage detail view.
     *
     * @param string $device encoded device name
     *
     * @return view
     */

    function view($device)
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');
        $this->load->library('storage/Storage');

        // Load view data
        //---------------

        try {
            $data['device'] = base64_decode(strtr($device, '-_.', '+/='));
            $data['details'] = $this->storage_device->get_device_details($data['device']);
            $data['types'] = $this->device_partition->get_file_system_types();
            $data['partition_types'] = $this->device_partition->get_partition_types();
            $data['remaining_space'] = $this->device_partition->get_remaining_space($data['device']);
            $data['storage_base'] = $this->storage->get_base();
            
            // Set default
            $data['type'] = 'ext4';
            $data['partition_type'] = 'primary';

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $this->page->view_form('devices/item', $data, lang('storage_store'));
    }

    /**
     * Creates a partiton.
     *
     * @param string $device encoded device name
     *
     * @return view
    */

    function create_device_partition($device)
    {

        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        try {

            $data['device'] = base64_decode(strtr($device, '-_.', '+/='));
            $data['types'] = $this->device_partition->get_file_system_types();
            $data['partition_types'] = $this->device_partition->get_partition_types();
            $data['details'] = $this->storage_device->get_device_details($data['device']);
            $data['remaining_space'] = $this->device_partition->get_remaining_space($data['device']);

            // Set default
            $data['type'] = 'ext4';
            $data['partition_type'] = 'primary';

            // Handle form submit
            //-------------------

            if ($this->input->post('submit')) {

                $partition_type = $this->input->post('partition_type');
                $file_type = $this->input->post('file_type');
                $partition_size = $this->input->post('partition_size');
                $is_first_partition = $this->input->post('first_partition');

                $this->device_partition->create_partition($device_decoded, $partition_type, $file_type, $partition_size, $is_first_partition);
                
                redirect('/storage/devices/view/'. $device);
                $this->page->set_status_added();

            }

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }
        // Load the views
        //---------------

        $this->page->view_form('devices/create_partition', $data, lang('storage_store'));
    }

    /**
     * Delete view.
     *
     * @param string $device encoded device name
     *
     * @return view
    */

    function delete($device)
    {

        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        // confirm uri
        //---------------

        $device_with_id = explode("=", $device_decoded);
        $device_encoded = strtr(base64_encode($device_with_id[0]),  '+/=', '-_.');
        $confirm_uri = '/app/storage/devices/destroy/' . $device;
        $cancel_uri = '/app/storage/devices/view/'.$device_encoded;

        $device_with_id = $device_with_id[0].'/'.$device_with_id[1];
        $items = array($device_with_id);
        $this->page->view_confirm_delete($confirm_uri, $cancel_uri, $items);
    }

    /**
     * Delete a partiton.
     *
     * @param string $device encoded device name
     *
     * @return redirect
    */

    function destroy($device)
    {
        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        try {
            $device_with_id = explode("=", $device_decoded);

            if(!empty($device_with_id)){
                $device_value = $device_with_id[0];
                $device_id = $device_with_id[1];
                $this->device_partition->delete_partition($device_value, $device_id);
                $this->page->set_status_deleted();
                $device_encoded = strtr(base64_encode($device_value),  '+/=', '-_.');
                redirect('/storage/devices/view/'. $device_encoded);
            }

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }
    }
    
    /**
    * Mount Partition
    *
    * @param string $device device
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function mount($device)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Mount Partition
        //----------------------------------------------
        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        redirect('/storage/devices/view/'. $device);
    }

    /**
    * Unount Partition
    *
    * @param string $device device
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function unmount($device)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Unount Partition
        //----------------------------------------------
        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        redirect('/storage/devices/view/'. $device);
    }
}
