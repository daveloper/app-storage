<?php

/**
 * Logical Volume controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Logical Volume controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

class Logical_Volume extends ClearOS_Controller
{
    /**
     * Logical volume overview.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage');
        $this->load->library('storage/Storage_Device');

        // Load view data
        //---------------

        try {
            $data['logical_volumes'] = $this->storage_device->get_logical_volume();

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $this->page->view_form('logical_volume/summary', $data, lang('storage_mappings'));
    }

    /**
    * Create Logical volume management.
    *
    * @return redirect
    */

    function create_lvm()
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Storage');

        try {
            $data = $this->storage_device->create_logical_volume_use_multi_devices();
            redirect('/storage');

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
    * Delete Logical volume management.
    *
    * @param String $lvolume Logical volume path
    *
    * @return redirect
    */

    function delete_lvm($lvolume)
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Storage');

        if ($this->input->post('submit')) {
            
            try {
                $device_decoded = base64_decode(strtr($lvolume, '-_.', '+/='));
                $logical_groups_name = $this->input->post('vg_name');

                $data = $this->storage_device->delete_logical_volume($device_decoded, $logical_groups_name);
                redirect('/storage');

            } catch (Exception $e) {

                $this->page->view_exception($e);
                return;
            }
        }  

    }
    
    /**
    * Reduse Logical volume management.
    *
    * @param String $lvolume Logical volume path
    *
    * @return redirect
    */

    function reduce_lvm($lvolume)
    {
        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Storage');

        $device_decoded = base64_decode(strtr($lvolume, '-_.', '+/='));

        if ($this->input->post('submit')) {
            
            try {
                $this->storage_device->reduce_logical_volume($device_decoded, $this->input->post('lv_size'), $this->input->post('lv_total_size'));
                redirect('/storage');

            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }    
    }
    
    /**
    * Extend Logical volume management.
    *
    * @param String $lvolume Logical volume path
    *
    * @return redirect
    */

    function extend_lvm($lvolume)
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Storage');

        // decode lvm

        $device_decoded = base64_decode(strtr($lvolume, '-_.', '+/='));

        if ($this->input->post('submit')) {
            
            $lv_size = $this->input->post('lv_size');
            $lv_total_size = $this->input->post('lv_total_size');
            
            try {
                $this->storage_device->extend_logical_volume($device_decoded, $lv_size, $lv_total_size);
                redirect('/storage');

            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }    
    }
}
